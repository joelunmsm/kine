import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    url='https://aniavestidos.com:5000/'

  headers:any={'Content-Type': 'text/html'}


  constructor(public http:HttpClient) { }



        kines(): Observable<any> {
    return this.http.get(this.url+'kinesphoto').pipe(
      map(results => results)
    );

   }


      kine(data): Observable<any> {
    return this.http.get(this.url+'kine/'+data).pipe(
      map(results => results)
    );

   }

      usuarios(): Observable<any> {
    return this.http.get(this.url+'usuarios').pipe(
      map(results => results)
    );

   }


  	crearoom(data): Observable<any> {

    return this.http.post(this.url+'crearoom',JSON.stringify(data)).pipe(
      map(results => results)
    );

   }

   creausuario(data): Observable<any> {

    return this.http.post(this.url+'creausuario',JSON.stringify(data)).pipe(
      map(results => results)
    );

   }

      creamensaje(data): Observable<any> {

    return this.http.post(this.url+'creamensaje',JSON.stringify(data)).pipe(
      map(results => results)
    );

   }

     mensajes(data): Observable<any> {
    return this.http.get(this.url+'mensajes/'+data).pipe(
      map(results => results)
    );

   }

        rooms(): Observable<any> {
    return this.http.get(this.url+'rooms').pipe(
      map(results => results)
    );

   }





}
