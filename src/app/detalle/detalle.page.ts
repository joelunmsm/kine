import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
 
detallekine:any={};
id:any;

link:any=window.location.href

  constructor(private route:ActivatedRoute,public dataService:DataService) { 


  	  this.route
      .queryParams
      .subscribe(params => {

      				this.id=JSON.parse(params['page'])

      				console.log(this.detallekine)

              this.dataService.kine(this.id).subscribe(d=>{

                this.detallekine=d[0]

              })




     			      		})



  }

  ngOnInit() {
  }

}
