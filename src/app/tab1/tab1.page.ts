import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

	kines:any={};

  constructor(public modalController: ModalController,private router: Router,public dataService:DataService) {


  	   this.kines=this.dataService.kines()




  }

  detalle(data){

  	this.router.navigate(['/tabs/tab1/detalle'], { queryParams:  {page:JSON.stringify(data['id'])} });

  }

 




}
