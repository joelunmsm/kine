import { Component,ViewChild } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DataService } from '../data.service';
import { ModalController } from '@ionic/angular';
import { UsuariosPage } from './usuarios/usuarios.page';
import { GruposPage } from './grupos/grupos.page';
import { AvatarPage } from '../avatar/avatar.page';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
	message = '';
	messages = [];
	currentUser = '';
	room:any='Chat';
	rooms:any;
	grupos:any;
  avatardestino:any;

avatarimagen:any;
@ViewChild(IonContent) ionContent: IonContent;

usuarios:any;

  constructor(public modalController: ModalController,public dataService:DataService,public alertController: AlertController,private socket: Socket, private toastCtrl: ToastController,private storage: Storage) {}

  ngOnInit() {



    this.dataService.rooms().subscribe(d=>{

      this.rooms=d

      this.grupos=this.rooms.filter(d=>d.tipo=='grupo')

    })


    this.socket.connect();


  	this.dataService.usuarios().subscribe(d=>{

  		this.usuarios = d

  	})

  	this.socket.emit('chat', this.room);

  	this.dataService.mensajes(this.room).subscribe(d=>{ this.messages=d })

    

 
    //let name = `user-${new Date().getTime()}`;
    //this.currentUser = name;
    
    //this.socket.emit('set-name', name);


    this.storage.get('usuario').then((val) => {
   

   if(val==null){

   	//this.alias()

     this.avatar()

   	   }
   	   else{


   this.currentUser = val['alias'];

   this.avatarimagen =val['avatar']

    this.socket.emit('set-name', val['alias']);


   	   }


  });
 
    this.socket.fromEvent('users-changed').subscribe(data => {
      let user = data['user'];
      if (data['event'] === 'left') {
        this.showToast('Chau: ' + user);
      } else {
        this.showToast('Bienvenido: ' + user);
      }
    });


      this.socket.fromEvent('users-room').subscribe(data => {
      let room = data['room'];

      
        this.showToast('Room joined: ' + room);
      
    });
 
    this.socket.fromEvent('message').subscribe(message => {

    	console.log('ssss',message)
      this.messages.push(message);

      this.messages = this.messages.filter(d=>d.chat==this.room)

    });
  }
 
  sendMessage() {


   

    this.storage.get('usuario').then((val) => {
   

   if(val==null){

   	//this.alias()

     this.avatar()

   	   }
   	   else{

   	   	
     this.socket.emit('send-message', { text: this.message });

    this.dataService.creamensaje({chat:this.room,msg:this.message,user:this.currentUser}).subscribe(d=>{})

    this.message = '';

    this.ionContent.scrollToBottom(300);

   	   }


  });



  }


      async avatar() {

    const modal = await this.modalController.create({
      component: AvatarPage,
      cssClass:"my-modal",
      backdropDismiss: false
    });

     modal.onDidDismiss()
      .then((data) => {

        console.log(data['data'])


        this.currentUser = data['data']['alias'];

        this.avatarimagen =data['data']['avatar']


        this.ionContent.scrollToBottom(1000);

    
        this.socket.emit('set-name', this.currentUser);

        this.dataService.creausuario(data['data']).subscribe(d=>{})

        this.storage.set('usuario', data['data']);
  

    });


    return  modal.present();


  }

 
  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }


    async presentModal() {

    const modal = await this.modalController.create({
      component: UsuariosPage,
      cssClass:"my-modal",
      backdropDismiss: false
    });

     modal.onDidDismiss()
      .then((data) => {

      		var _data = data['data']['usuario']

      		if (data['data']['usuario']!='cierra'){
        
      		this.room =_data+'_'+this.currentUser

            this.avatardestino=data['data']

          	this.room = this.room.split('_').sort();

          	this.room = this.room[0]+'_'+this.room[1]

          	console.log('room...',this.room)

            this.socket.emit('chat', this.room );  

            this.dataService.crearoom(this.room).subscribe(d=>{})

            this.dataService.mensajes(this.room).subscribe(d=>{

            		console.log('messages',d)

            		this.messages=d

                this.ionContent.scrollToBottom(300);


            })

          }

  

    });


    return  modal.present();


  }




async traegrupos(){

    const modal = await this.modalController.create({
      component: GruposPage,
      cssClass:"my-modal",
      backdropDismiss: false
    });

     modal.onDidDismiss()
      .then((data) => {

      		if (data['data']['grupo']!='cierra'){

				this.room =data['data']['grupo']

				this.socket.emit('chat', this.room);

				this.dataService.mensajes(this.room).subscribe(d=>{

					console.log('messages',d)

				this.messages=d
        this.ionContent.scrollToBottom(300);

        this.avatardestino=null


				})
      		}
            
  

    });


    return  modal.present();


}

escogeroom(d){

  console.log(d['room'])

    this.room =d['room']

        this.socket.emit('chat', this.room);

        this.dataService.mensajes(this.room).subscribe(d=>{

          console.log('messages',d)

        this.messages=d


        })

}



 




}
