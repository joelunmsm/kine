import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { DataService } from '../../data.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.page.html',
  styleUrls: ['./grupos.page.scss'],
})
export class GruposPage implements OnInit {


rooms:any;
grupos:any;
 constructor(public modalController: ModalController,public dataService:DataService,public alertController: AlertController,private socket: Socket, private toastCtrl: ToastController,private storage: Storage) {}

  ngOnInit() {


this.traegrupos()

  }




  traegrupos(){

  		this.dataService.rooms().subscribe(d=>{


  		this.rooms=d
  		this.grupos=this.rooms.filter(d=>d.tipo=='grupo')



  	})

  }

     closeModal(){


    this.modalController.dismiss({
      'grupo': 'cierra',
    });


}

escoge(d){


    this.modalController.dismiss({
      'grupo': d.room,
    });


}


   async creagrupo() {
    const alert = await this.alertController.create({
      header: 'Grupo',
      inputs: [
        {
          name: 'alias',
          type: 'text',
          label: 'Grupo',
          placeholder: 'Ingrese grupo',
          checked: true
        },

       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok',data);

            this.dataService.crearoom(data['alias']).subscribe(d=>{})

            this.traegrupos()

          }
        }
      ]
    });

    await alert.present();
  }


}
