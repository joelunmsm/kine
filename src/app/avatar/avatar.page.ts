import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.page.html',
  styleUrls: ['./avatar.page.scss'],
})
export class AvatarPage implements OnInit {

  avatar=[{imagen:'001-man.png'},{imagen:'002-man.png'},{imagen:'003-man.png'},{imagen:'004-man.png'},{imagen:'005-man.png'},{imagen:'006-man.png'},{imagen:'007-man.png'},{imagen:'008-man.png'},{imagen:'009-man.png'},{imagen:'010-woman.png'},{imagen:'011-woman.png'},{imagen:'012-woman.png'},{imagen:'013-woman.png'},{imagen:'014-woman.png'},{imagen:'015-woman.png'},{imagen:'016-woman.png'},{imagen:'017-woman.png'},{imagen:'018-man.png'},{imagen:'019-man.png'},{imagen:'020-man.png'},{imagen:'021-man.png'},{imagen:'022-man.png'},{imagen:'023-man.png'},{imagen:'024-man.png'},{imagen:'025-man.png'},{imagen:'026-man.png'},{imagen:'027-woman.png'},{imagen:'028-woman.png'},{imagen:'029-woman.png'},{imagen:'030-woman.png'},{imagen:'031-woman.png'},{imagen:'032-woman.png'},{imagen:'033-woman.png'},{imagen:'034-woman.png'},{imagen:'035-man.png'},{imagen:'036-man.png'},{imagen:'037-man.png'},{imagen:'038-man.png'},{imagen:'039-man.png'},{imagen:'040-man.png'},{imagen:'041-man.png'},{imagen:'042-man.png'},{imagen:'043-woman.png'},{imagen:'044-woman.png'},{imagen:'045-woman.png'},{imagen:'046-woman.png'},{imagen:'047-woman.png'},{imagen:'048-woman.png'},{imagen:'049-woman.png'},{imagen:'050-woman.png'}]
 
 	alias:any
 	aliasescogido:any;

  constructor(public modalController: ModalController,public toastController: ToastController) { }

  ngOnInit() {
  }

  escogeavatar(data){

  	console.log(data)

  	this.avatar.map(d=>{
  		d['hasavatar']=false
  		return d
  	})

  	data.hasavatar=data.hasavatar?false:true

  	this.aliasescogido=this.avatar.filter(d=>d['hasavatar']==true)[0]['imagen']
  }


  guarda(){


  	if(this.alias!=undefined && this.aliasescogido!=undefined){

  


    this.modalController.dismiss({
      'alias': this.alias,
      'avatar':this.aliasescogido    });



  	}
  	else{

  		this.presentToast('Ingrese un alias y un avatar')
  	}


  }


   async presentToast(data) {
    const toast = await this.toastController.create({
      message: data,
      duration: 2000
    });
    toast.present();
  }

}
